package com.example.marcusviniciussilvaferreira_trab;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class FuncoesBD {
    private SQLiteDatabase db;
    private CriarBD banco;

    public FuncoesBD(Context context){
        banco = new CriarBD(context);
    }

    public String InsertDados(String nome, String email, String sexo, String phone){
        ContentValues valores;
        long resultado;

        db = banco.getWritableDatabase();
        valores = new ContentValues();
        valores.put(CriarBD.NOME, nome);
        valores.put(CriarBD.EMAIL, email);
        valores.put(CriarBD.SEXO, sexo);
        valores.put(CriarBD.TELEFONE, phone);

        resultado = db.insert(CriarBD.TABELA, null, valores);
        db.close();

        if (resultado ==-1)
            return "Erro ao inserir registro";
        else
            return "Registro Inserido com sucesso";
    }

    public Cursor CarregaDados(){
        Cursor cursor;
        String[] campos = { CriarBD.ID, CriarBD.NOME, CriarBD.EMAIL, CriarBD.TELEFONE, CriarBD.SEXO };
        db = banco.getReadableDatabase();
        cursor = db.query(CriarBD.TABELA, campos, null, null, null, null, null, null);
        if(cursor != null) {
            cursor.moveToFirst();
        }
        db.close();
        return cursor;
    }

    public Cursor CarregaById(int id){
        Cursor cursor;
        String[] campos =  {CriarBD.ID, CriarBD.NOME, CriarBD.EMAIL, CriarBD.TELEFONE};
        String where = CriarBD.ID + "=" + id;
        db = banco.getReadableDatabase();
        cursor = db.query(CriarBD.TABELA,campos,where, null, null, null, null, null);

        if(cursor!=null){
            cursor.moveToFirst();
        }
        db.close();
        return cursor;
    }
    public void AlteraById(int id, String nome, String mail, String phone){
        ContentValues valores;
        String where;
        db = banco.getWritableDatabase();
        where = CriarBD.ID + "=" + id;
        valores = new ContentValues();
        valores.put(CriarBD.NOME, nome);
        valores.put(CriarBD.EMAIL, mail);
        valores.put(CriarBD.TELEFONE, phone);
        db.update(CriarBD.TABELA, valores, where,null);
        db.close();
    }
    public void DeleteById(int id){
        String where = CriarBD.ID + "=" + id;
        db = banco.getReadableDatabase();
        db.delete(CriarBD.TABELA, where,null);
        db.close();
    }
}
