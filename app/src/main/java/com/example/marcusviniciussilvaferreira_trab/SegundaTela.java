package com.example.marcusviniciussilvaferreira_trab;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class SegundaTela extends AppCompatActivity {
    private ListView lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle(Common.SEGUNDA_TELA);
        setContentView(R.layout.activity_segunda_tela);

        FuncoesBD crud = new FuncoesBD(getBaseContext());
        Cursor cursor = crud.CarregaDados();

        String[] nomeCampos = new String[] {CriarBD.ID, CriarBD.NOME};
        int[] idViews = new int[] {R.id.id, R.id.idNome};

        SimpleCursorAdapter adaptador = new SimpleCursorAdapter(getBaseContext(),
                R.layout.activity_listview, cursor, nomeCampos, idViews,0);
        lista = (ListView)findViewById(R.id.listView);
        lista.setAdapter(adaptador);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String codigo;
                cursor.moveToPosition(position);
                codigo = cursor.getString(cursor.getColumnIndexOrThrow(CriarBD.ID));
                Intent intent = new Intent(getBaseContext(), Editar.class);
                intent.putExtra("codigo", codigo);
                startActivity(intent);
                finish();
            }
        });
    }

    public void Voltar(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}