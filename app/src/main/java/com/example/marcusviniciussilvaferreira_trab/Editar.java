package com.example.marcusviniciussilvaferreira_trab;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class Editar extends AppCompatActivity{
    String codigo = "";
    EditText nome;
    EditText email;
    EditText telefone;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edita);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        codigo = bundle.getString("codigo");

        nome = (EditText)findViewById(R.id.txtNome2);
        email = (EditText)findViewById(R.id.txtEmail2);
        telefone = (EditText)findViewById(R.id.txtTelefone2);

        FuncoesBD banco = new FuncoesBD(getBaseContext());
        Cursor cursor = banco.CarregaById(Integer.parseInt(codigo));
        String nomeAux = cursor.getString(cursor.getColumnIndexOrThrow(CriarBD.NOME));
        String emailAux = cursor.getString(cursor.getColumnIndexOrThrow(CriarBD.EMAIL));
        String phoneAux = cursor.getString(cursor.getColumnIndexOrThrow(CriarBD.TELEFONE));
        nome.setText(nomeAux);
        email.setText(emailAux);
        telefone.setText(phoneAux);
    }

    public void Editar(View view)
    {
        FuncoesBD banco = new FuncoesBD(getBaseContext());
        String nome = this.nome.getText().toString();
        String email = this.email.getText().toString();
        String phone = this.telefone.getText().toString();
        banco.AlteraById(Integer.parseInt(codigo), nome, email, phone);
        Intent intent = new Intent(this, MainActivity.class);
        finish();
        startActivity(intent);
    }

    public void Deletar(View view)
    {
        FuncoesBD banco = new FuncoesBD(getBaseContext());
        banco.DeleteById(Integer.parseInt(codigo));
        Intent intent = new Intent(this, MainActivity.class);
        finish();
        startActivity(intent);
    }
}
