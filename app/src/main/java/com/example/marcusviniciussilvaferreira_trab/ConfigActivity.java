package com.example.marcusviniciussilvaferreira_trab;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class ConfigActivity extends AppCompatActivity
{
    private EditText Variavel_Um;
    private EditText Variavel_Dois;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configura);
        Common app = (Common)getApplicationContext();
        Variavel_Um = (EditText)findViewById(R.id.txtFirst);
        Variavel_Dois = (EditText)findViewById(R.id.txtSecond);
        Variavel_Um.setText(app.PRIMEIRA_TELA);
        Variavel_Dois.setText(app.SEGUNDA_TELA);
    }

    public void ArmazenaConfiguracoes(View view)
    {
        Common.PRIMEIRA_TELA = Variavel_Um.getText().toString();
        Common.SEGUNDA_TELA = Variavel_Dois.getText().toString();
    }

    public void REtorna(View view)
    {
        setContentView(R.layout.activity_main);
    }
}