package com.example.marcusviniciussilvaferreira_trab;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

public class MainActivity extends AppCompatActivity {
    private EditText m_EditName;
    private EditText m_EditPhone;
    private EditText m_EditMail;
    private RadioButton m_RbMasc;
    private RadioButton m_RbFem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setTitle(com.example.marcusviniciussilvaferreira_trab.Common.PRIMEIRA_TELA);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        m_EditName = (EditText) findViewById(R.id.txtNome);
        m_EditPhone = (EditText) findViewById(R.id.txtTelefone);
        m_EditMail = (EditText) findViewById(R.id.txtEmail);
        m_RbMasc = (RadioButton) findViewById(R.id.OptionMasc);
        m_RbFem = (RadioButton) findViewById(R.id.optionFem);
    }

    public void VerCadastro(View view) {
        Intent intent = new Intent(this, SegundaTela.class);
        Bundle bundle = new Bundle();
        bundle.putString("name", m_EditName.getText().toString());
        bundle.putString("phone", m_EditPhone.getText().toString());
        bundle.putString("mail", m_EditMail.getText().toString());
        bundle.putBoolean("masc", m_RbMasc.isChecked());
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void VerCadastros(View view)
    {
        Intent intent = new Intent(this, SegundaTela.class);
        startActivity(intent);
    }

    public void Cadastrar(View view)
    {
        FuncoesBD banco = new FuncoesBD(getBaseContext());
        String nome = m_EditName.getText().toString();
        String phone = m_EditPhone.getText().toString();
        String mail = m_EditMail.getText().toString();
        String sexo = m_RbMasc.isChecked() ? "MASC" : "FEM";
        banco.InsertDados(nome, mail, sexo, phone);
    }
}